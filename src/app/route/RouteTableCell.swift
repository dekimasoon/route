//
//  RouteTableCell.swift
//  route
//
//  Created by 今井 響 on 3/19/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

class RouteTableCell: UITableViewCell {
    var nameLabel: UILabel? = UILabel(frame: CGRectMake(120, 10, 325, 30))
    var descriptionLabel: UILabel? = UILabel(frame: CGRectMake(120, 40, 325, 40))
    var iconImg: UIImageView? = UIImageView(frame: CGRectMake(10, 10, 90, 80))
    
    required init(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setCell(item: RouteItem) {
        self.nameLabel!.text = item.name
        self.descriptionLabel!.text = item.description
        self.iconImg!.image = item.thumnail
        
        self.nameLabel!.numberOfLines = 1
        self.descriptionLabel!.numberOfLines = 0
        self.descriptionLabel!.sizeToFit()
        
        self.addSubview(self.nameLabel!)
        self.addSubview(self.descriptionLabel!)
        self.addSubview(self.iconImg!)
    }
}
