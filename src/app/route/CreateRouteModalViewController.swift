//
//  CreateRouteModalViewController.swift
//  route
//
//  Created by 今井 響 on 3/27/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

protocol ModalViewControllerDelegate{
    func modalDidFinished(item: RouteItem)
}

class CreateRouteModalViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var delegate: ModalViewControllerDelegate! = nil
    var tableView = UITableView(frame: CGRectMake(0, 30, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height))
    var items: [RouteItem]? = RouteItem.loadItems()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        tableViewSetting()
        tableView.reloadData()
        self.view.addSubview(tableView)
        
        self.view.layoutIfNeeded()
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = RouteTableCell(style: UITableViewCellStyle.Default, reuseIdentifier: "data")
        cell.setCell(items![indexPath.row])
        
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func tableView(talbeView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.delegate.modalDidFinished(items![indexPath.row])
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cell = self.tableView(tableView, cellForRowAtIndexPath: indexPath) as RouteTableCell
        var description = items![indexPath.row].description
        var maxSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width, CGFloat.max)
        var attr = [NSFontAttributeName: UIFont.boldSystemFontOfSize(18)]
        var modifiedSize = description.boundingRectWithSize(maxSize, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attr, context: nil).size
        
        return max(modifiedSize.height + 20, 100)
    }
    
    func tableViewSetting() {
        tableView.registerClass(RouteTableCell.classForCoder(), forCellReuseIdentifier: "data")
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}