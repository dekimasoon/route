//
//  SpotDetailViewController.swift
//  route
//
//  Created by 今井 響 on 3/17/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

class RouteDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var items: [RouteItem]? = RouteItem.loadItems()
    var tableView: UITableView = UITableView(frame: CGRectMake(0, 20, 360, 580))
    var mapView = RouteDetailMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        tableViewSetting()
        self.view.addSubview(tableView)
        
        var markers: [GMSMarker]? = [GMSMarker]()
        
        for (var i = 0; i < items!.count; i++){
            RouteDetailMapView.addMarker(items![i], markers: &markers!, mapView: self.mapView)
        }
        
        self.mapView.createMapView(items!)
        
        //ルートのタイトル、説明と共に、引数のmapViewをaddSubViewした状態のheaderViewを得る
        var headerView: UIView = RouteTableHeaderView.makeHeaderView(mapView)
        RouteDetailMapView.adjustCamera(markers!, mapView: mapView)
        tableView.tableHeaderView = headerView
        
        var toSpotListButton = UIButton(frame: CGRectMake(UIScreen.mainScreen().bounds.size.width/2 - 100, 610, 200, 30))
        toSpotListButton.setTitle("スポットリスト", forState: .Normal)
        toSpotListButton.backgroundColor = UIColor.blackColor()
        toSpotListButton.addTarget(self, action: "onClickToSpotListButton:", forControlEvents: .TouchUpInside)
        self.view.addSubview(toSpotListButton)
        

    }
    
    // ボタンクリック時のイベント
    func onClickToSpotListButton(sender: UIButton) {
        let firstViewController = SpotListViewController()
        firstViewController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(firstViewController, animated: true, completion: nil)
    }
    
    // TableViewのセルの数を返す（定義必須）
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items!.count
    }
    
    //TableViewのCellの内容記述（定義必須）
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = RouteTableCell(style: UITableViewCellStyle.Default, reuseIdentifier: "data")
        cell.setCell(items![indexPath.row])
        
        //セルの高さ自動調節のため
        cell.layoutIfNeeded()
        
        return cell
    }
    
    // セルの生成
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cell = self.tableView(tableView, cellForRowAtIndexPath: indexPath) as RouteTableCell
        var description = items![indexPath.row].description
        var maxSize = CGSizeMake(245, CGFloat.max)
        var attr = [NSFontAttributeName: UIFont.boldSystemFontOfSize(18)]
        var modifiedSize = description.boundingRectWithSize(maxSize, options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: attr, context: nil).size
        
        return max(modifiedSize.height + 20, 100)
    }
    
    //tableViewに関する決まり文句的な記述を切り出し
    func tableViewSetting() {
        
        //"data"はセルと接続する際の識別名
        tableView.registerClass(RouteTableCell.classForCoder(), forCellReuseIdentifier: "data")
        tableView.dataSource = self
        tableView.delegate = self
        
        //セルの高さを自動調節するための記述。estimatedRowHeightはパフォーマンス向上にもつながる
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}