//
//  RegisterSpotView.swift
//  route
//
//  Created by 今井 響 on 3/22/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

class RegisterSpotView: UIView {
    
    class func createTitleEditView(toDelegateCV: RegisterSpotViewController) -> UIView {
        var titleEditView = UIView(frame: CGRectMake(0, 30, 360, 100))
        
        // 「スポット名」と表示するラベルの作成
        var titleLabel = UILabel(frame: CGRectMake(UIScreen.mainScreen().bounds.size.width/2 - 150, 10, 300, 30))
        titleLabel.text = "スポット名"
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.textAlignment = NSTextAlignment.Center
        titleEditView.addSubview(titleLabel)
        
        // スポット名を入力させる欄の作成
        let titleField: UITextField = UITextField(frame: CGRectMake(UIScreen.mainScreen().bounds.size.width/2 - 150, 50, 300, 100))
        titleField.borderStyle = UITextBorderStyle.RoundedRect
        titleField.delegate = toDelegateCV
        titleField.tag = 1
        titleEditView.addSubview(titleField)
        
        return titleEditView
    }
    
    class func createDescriptionEditView(toDelegateCV: RegisterSpotViewController) -> UIView {
        var descriptionEditView = UIView(frame: CGRectMake(0, 150, 360, 200))
        
        // 「スポットの説明」と表示するラベルの作成
        var descriptionLabel = UILabel(frame: CGRectMake(UIScreen.mainScreen().bounds.size.width/2 - 150, 0, 300, 30))
        descriptionLabel.text = "スポットの説明"
        descriptionLabel.textColor = UIColor.blackColor()
        descriptionLabel.textAlignment = NSTextAlignment.Center
        descriptionEditView.addSubview(descriptionLabel)
        
        // スポットの説明を入力させる欄の作成
        var descriptionField = UITextView(frame: CGRectMake(UIScreen.mainScreen().bounds.size.width/2 - 150, 50, 300, 150))
        descriptionField.layer.borderColor = UIColor.grayColor().CGColor
        descriptionField.layer.masksToBounds = true
        descriptionField.layer.cornerRadius = 8.0
        descriptionField.layer.borderWidth = 0.5
        descriptionField.font = UIFont.boldSystemFontOfSize(12)
        descriptionField.delegate = toDelegateCV
        descriptionField.tag = 2
        descriptionEditView.addSubview(descriptionField)
        
        return descriptionEditView
    }
}