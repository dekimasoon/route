//
//  SpotListHeaderView.swift
//  route
//
//  Created by 今井 響 on 3/20/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

class SpotListHeaderView: UIView{
    class func makeHeaderView() -> UIView {
        var headerView = UIView(frame: CGRectMake(0, 0, 360, 80))
        var headerTitleLabel = UILabel(frame: CGRectMake(15, 15, 345, 20))
        headerTitleLabel.text = "スポット一覧"
        headerTitleLabel.font = UIFont.systemFontOfSize(24)
        headerTitleLabel.textAlignment = NSTextAlignment.Center
        headerTitleLabel.textColor = UIColor.blackColor()
        headerTitleLabel.numberOfLines = 0
        headerTitleLabel.sizeToFit()
        headerView.addSubview(headerTitleLabel)
        
        return headerView
    }
}