//
//  DataManager.swift
//  route
//
//  Created by Manni on 3/18/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import UIKit
import Realm

class DataManager: NSObject {
    
    // API URL
    class var apiUrlBase: String {
        get {
            return "http://route-loadbalancer-public-1406540833.ap-northeast-1.elb.amazonaws.com/api/v1/"
        }
    }
    
    // updateNewArrivalRouteList()
    class func updateNewArrivalRouteList(completionHandler:(result: RLMResults) -> Void) {
        
        let realmName = "NewArrivalRouteList"
        let realm = RealmManager.getRealm(realmName)
        let result = Route.allObjectsInRealm(realm)
        
        if (!RealmManager.isNeedUpdate(realmName, key: nil)) {
            completionHandler(result: result)
            return
        }
        
        let url = apiUrlBase + "routes"
        request(.GET, url)
            .responseSwiftyJSON { (request, response, json, error) in
                
                if (error != nil) {
                    println(error)
                    completionHandler(result: result)
                    return
                }
                
                realm.beginWriteTransaction()
                realm.deleteAllObjects()
                for (index, data) in json["routes"] {
                    let route = Route(json: data)
                    Route.createOrUpdateInRealm(realm, withObject: route)
                }
                realm.commitWriteTransaction()
                completionHandler(result: result)
        }
    }
    
    // updateRouteDetail(user, routeId)
    class func updateRouteDetail(userId: String, routeId: String, completionHandler:(result: RLMResults) -> Void) {
        
        let realmName = "DetailRealm"
        let realm = RealmManager.getRealm(realmName)
        let result = Route.allObjectsInRealm(realm)
        
        if (!RealmManager.isNeedUpdate(realmName, key: routeId)) {
            completionHandler(result: result)
            return
        }
        
        let url = apiUrlBase + "users/" + userId + "/routes/" + routeId
        request(.GET, url)
            .responseSwiftyJSON { (request, response, json, error) in
                
                if (error != nil) {
                    println(error)
                    completionHandler(result: result)
                    return
                }
                
                realm.beginWriteTransaction()
                realm.deleteAllObjects()
                for (index, data) in json["routes"] {
                    let route = Route(json: data)
                    Route.createOrUpdateInRealm(realm, withObject: route)
                }
                realm.commitWriteTransaction()
                completionHandler(result: result)
        }
    }
    
    
    // updateUserRouteList(userid)
    class func updateUserRouteList(userId: String, completionHandler:(result: RLMResults) -> Void) {
        
        let realmName = "UserRouteList"
        let realm = RealmManager.getRealm(realmName)
        let result = Route.allObjectsInRealm(realm)
        
        if (!RealmManager.isNeedUpdate(realmName, key: userId)) {
            completionHandler(result: result)
            return
        }
        
        let url = apiUrlBase + "users/" + userId + "/routes"
        request(.GET, url)
            .responseSwiftyJSON { (request, response, json, error) in
                
                if (error != nil) {
                    println(error)
                    completionHandler(result: result)
                    return
                }
                
                realm.beginWriteTransaction()
                realm.deleteAllObjects()
                for (index, data) in json["routes"] {
                    let route = Route(json: data)
                    Route.createOrUpdateInRealm(realm, withObject: route)
                }
                realm.commitWriteTransaction()
                completionHandler(result: result)
        }
    }
    
    // updateUserSpotList(userId)
    class func updateUserSpotList(userId: String, completionHandler:(result: RLMResults) -> Void) {
        
        let realmName = "UserSpotList"
        let realm = RealmManager.getRealm(realmName)
        let result = Spot.allObjectsInRealm(realm)
        
        if (!RealmManager.isNeedUpdate(realmName, key: userId)) {
            completionHandler(result: result)
            return
        }
        
        let url = apiUrlBase + "users/" + userId + "/spots"
        request(.GET, url)
            .responseSwiftyJSON { (request, response, json, error) in
                
                if (error != nil) {
                    println(error)
                    completionHandler(result: result)
                    return
                }
                
                realm.beginWriteTransaction()
                realm.deleteAllObjects()
                for (index, data) in json["spots"] {
                    let spot = Spot(json: data)
                    Spot.createOrUpdateInRealm(realm, withObject: spot)
                }
                realm.commitWriteTransaction()
                completionHandler(result: result)
        }
    }
}
