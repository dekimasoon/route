//
//  SpotListViewController.swift
//  route
//
//  Created by 今井 響 on 3/20/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

class SpotListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    let screenWidth = UIScreen.mainScreen().bounds.width
    let screenHeight = UIScreen.mainScreen().bounds.height
    var items: [RouteItem]? = RouteItem.loadItems()
    var tableView: UITableView = UITableView(frame: CGRectMake(0, 20, 360, 500))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        tableViewSetting()
        self.view.addSubview(tableView)
        
        var headerView = SpotListHeaderView.makeHeaderView()
        
        var toRegisterSpotButton = UIButton(frame: CGRectMake(screenWidth/2 - 50, 550, 100, 30))
        toRegisterSpotButton.setTitle("新規追加", forState: .Normal)
        toRegisterSpotButton.backgroundColor = UIColor.blackColor()
        toRegisterSpotButton.addTarget(self, action: "onClickToRegisterSpotButton:", forControlEvents: .TouchUpInside)
        self.view.addSubview(toRegisterSpotButton)
        
        tableView.tableHeaderView = headerView
        
        var toRouteDetailButton = UIButton(frame: CGRectMake(screenWidth/2 - 100, 610, 200, 30))
        toRouteDetailButton.setTitle("ルート詳細", forState: .Normal)
        toRouteDetailButton.backgroundColor = UIColor.blackColor()
        toRouteDetailButton.addTarget(self, action: "onClickToRouteDetailButton:", forControlEvents: .TouchUpInside)
        
        self.view.addSubview(toRouteDetailButton)
        
    }
    
    // ボタンクリック時の画面遷移イベント
    func onClickToRouteDetailButton(sender: UIButton) {
        let secondViewController: UIViewController = RouteDetailViewController()
        secondViewController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(secondViewController, animated: true, completion: nil)
    }
    
    func onClickToRegisterSpotButton(sender: UIButton) {
        let registerSpotViewController = RegisterSpotViewController()
        registerSpotViewController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(registerSpotViewController, animated: true, completion: nil)
    }
    
    // セルクリック時の画面遷移イベント
    func onClickToRouteDetailButton(sender: AnyObject!) {
        let secondViewController: UIViewController = RouteDetailViewController()
        secondViewController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(secondViewController, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
    
        var cell = RouteTableCell(style: UITableViewCellStyle.Default, reuseIdentifier: "data")
        cell.setCell(items![indexPath.row])
        
        cell.layoutIfNeeded()
        
        return cell
    }
    
    // セルの生成
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        let cell = self.tableView(tableView, cellForRowAtIndexPath: indexPath) as RouteTableCell
        var description = items![indexPath.row].description
        var maxSize = CGSizeMake(screenWidth, CGFloat.max)
        var attr = [NSFontAttributeName: UIFont.boldSystemFontOfSize(18)]
        var modifiedSize = description.boundingRectWithSize(maxSize, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attr, context: nil).size
        
        return max(modifiedSize.height + 20, 100)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        self.onClickToRouteDetailButton(self)
    }
    
    // tableViewに関する決まり文句、設定など
    func tableViewSetting() {
        
        tableView.frame = CGRectMake(0, 20, screenWidth, (screenHeight - 20)*0.8)
        tableView.registerClass(RouteTableCell.classForCoder(), forCellReuseIdentifier: "data")
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}