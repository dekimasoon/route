//
//  RouteItem.swift
//  route
//
//  Created by 今井 響 on 3/17/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

//ルートを構成する各スポットのクラス。セルに入れる用
class RouteItem {
    var name: String
    var description: String
    var thumnail: UIImage?
    var position: CLLocationCoordinate2D?
    
    init(name: String, description: String, thumnail: UIImage?, position: CLLocationCoordinate2D?){
        self.name = name
        self.description = description
        self.thumnail = thumnail!
        self.position = position!
    }
    
    init() {
        self.name = ""
        self.description = ""
        self.thumnail = nil
        self.position = nil
    }
    
    class func loadItems() -> [RouteItem]? {
        
        var items: [RouteItem]? = [RouteItem]()
        
        for (var i = 0; i < 5; i++){
            
            var name = "spot\(i)"
            var description = "これはスポット\(i)です"
            var thumnail = UIImage(named: "thumnail\(i).jpg")
            
            //画像のリサイズ
            let size = CGSizeMake(80, 60)
            UIGraphicsBeginImageContext(size)
            thumnail!.drawInRect(CGRectMake(0, 0, size.width, size.height))
            thumnail = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            //スポットの位置を適当に作る
            var delta = Float(i) * 0.003
            var x = Float(139.631000)
            var latitude = CLLocationDegrees(35.779000)
            var longitude = CLLocationDegrees(x + delta)
            
            // geohashのテスト
            var hash: NSString = GeoHash.hashForLatitude(latitude, longitude: longitude, length: 13)
            println("hash:\(hash)")
            var area: GHArea = GeoHash.areaForHash(hash)
            var position = CLLocationCoordinate2D(latitude:CLLocationDegrees(area.latitude.max),
                                                    longitude: CLLocationDegrees(area.longitude.max))
            
            var item = RouteItem(name: name, description: description, thumnail: thumnail!, position: position)
            items!.append(item)
        }
        
        //テーブルビューのセルの高さ自動調整を確認するために、スポット名が長いものを追加
        var thumnail: UIImage? = UIImage(named: "thumnail2.jpg")
        let size = CGSizeMake(80, 60)
        UIGraphicsBeginImageContext(size)
        thumnail!.drawInRect(CGRectMake(0, 0, size.width, size.height))
        thumnail = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        var position = CLLocationCoordinate2D(latitude: 35.777000, longitude: 139.630000)
        
        items!.append(RouteItem(name: "ほわっちゃほい",
            description: "あびょおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおお",
            thumnail: thumnail!, position: position))
        return items
    }
}

