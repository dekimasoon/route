'use strict';

module.exports = {
  idGenerator: require('./idGenerator'),
  token: require('./token')
};