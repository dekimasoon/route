'use strict';

var express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    utils = require('./utils');

var app = express();
app.use(bodyParser.json());
app.use(cors());

app.use(function(req, res, next) {
  console.log('%s %s', req.method, req.url);
  next();
});

var router = express.Router();
// アルファでは認証を無効に
//router.use(utils.token.verify({ unless: ['POST:/token', 'POST:/users'] }));
router.use('/token', require('./routers/token'));
router.use('/users', require('./routers/users'));
router.use('/routes', require('./routers/routes'));
router.use('/spots', require('./routers/spots'));

app.use('/api/v1', router);

app.listen(3000, function() {
  console.log('Server starts on port: %s', 3000);
});



