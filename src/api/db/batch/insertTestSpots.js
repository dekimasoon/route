'use strict';

var vogels = require('vogels'),
    AWS    = vogels.AWS,
    Joi    = require('joi'),
    rand   = require('rand-token'),
    async  = require('async'),
    geohash = require('ngeohash'),
    User   = vogels.define('User', require('./../models/user')),
    Spot   = vogels.define('Spot', require('./../models/spot'));

Array.prototype.random = function() {
  return this[Math.floor(Math.random()*this.length)];
};

var generateTestSpot = function(users) {

  var lat = Math.floor((35.77 + Math.random() / 10) * 1000000) / 1000000;
  var lng = Math.floor((139.62 + Math.random() / 10) * 1000000) / 1000000;
  var hash10 = geohash.encode(lat, lng, 10);
  var hash4 = geohash.encode(lat, lng, 4);

  return {
    userId: users.random().attrs.userId,
    spotId: 's' + rand.uid(12),
    title: ['自慢の','ホッとする','如何わしい'].random() + ['公園','近道','夕日スポット','カフェ','掲示板'].random(),
    image: 'https://s3-ap-northeast-1.amazonaws.com/ibeya.route/spotimage/0' + Math.ceil(Math.random()*6) + '.jpg',
    geohash4: hash4,
    geohash10: hash10,
    isPublished: true,
    createdAt: new Date(),
    updatedAt: new Date()
  };

};

User.scan().exec(function(err, resp) {

  if (err) {
    console.log(err);
    return;
  }

  var users = resp.Items;

  async.times(25, function(n, next) {
    Spot.create(generateTestSpot(users), function(error) {
      if (error) {
        console.log(error);
      } else {
        next();
      }
    });
  });
});







