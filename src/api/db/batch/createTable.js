'use strict';

var vogels = require('vogels'),
    AWS = vogels.AWS,
    Joi = require('joi');

AWS.config.loadFromPath('../.aws.credentials.json');

vogels.define('User', require('./../models/user'));
vogels.define('Route', require('./../models/route'));
vogels.define('Spot', require('./../models/spot'));
vogels.define('RouteSpot', require('./../models/routeSpot'));

vogels.createTables({
  'User': {readCapacity: 1, writeCapacity: 1},
  'Route': {readCapacity: 1, writeCapacity: 1},
  'Spot': {readCapacity: 1, writeCapacity: 1},
  'RouteSpot': {readCapacity: 1, writeCapacity: 1}
}, function(err) {
  if(err) {
    console.log('Error creating tables', err);
  } else {
    console.log('table are now created and active');
  }
});
