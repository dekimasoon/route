'use strict';

var router = require('express').Router(),
    rand   = require('rand-token'),
    db     = require('../../db');

router.route('/')
  .get(function(req, res, next) {

    db.Spot
      .query(req.user.id)
      .exec(function(err, result) {

        if (err || !result.Items.length) {
          res.status(400).end();
          return;
        }

        var data = {
          spots: []
        };

        result.Items.forEach(function(item) {
          data.spots.push(item.attrs);
        });
        res.json(data);

      });
  })
  .post(function(req, res, next) {

    console.log(req.body);

    var spot = {
      userId: req.user.id,
      spotId: 's' + rand.uid(12),
      title: req.body.title,
      image: req.body.image,
      geohash4: req.body.geohash10.slice(0, 4),
      geohash10: req.body.geohash10,
      isPublished: req.body.isPublished,
      createdAt: new Date(),
      updatedAt: new Date()
    };

    db.Spot.create(spot, function(err) {

      if (err) {
        console.log(err);
        res.status(400).end();
        return;
      }
      res.json(spot);

    });
  });

module.exports = router;