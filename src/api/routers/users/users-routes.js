'use strict';

var router = require('express').Router(),
    rand   = require('rand-token'),
    db = require('../../db');

router.route('/')
  .get(function(req, res, next) {

    db.Route
      .query(req.user.id)
      .exec(function(err, result) {

        if (err || !result.Items.length) {
          res.status(400).end();
          return;
        }

        var data = {
          routes: []
        };

        result.Items.forEach(function(item) {
          data.routes.push(item.attrs);
        });
        res.json(data);

      });
  })
  .post(function(req, res, next) {

    console.log(req.body);

    var route = {
      userId: req.user.id,
      routeId: 'r' + rand.uid(12),
      title: req.body.title,
      desc: req.body.desc,
      image: req.body.image,
      geohash4: req.body.geohash10.slice(0, 4),
      geohash10: req.body.geohash10,
      isPublished: req.body.isPublished,
      createdAt: new Date(),
      updatedAt: new Date()
    };

    db.Route.create(route, function(err) {

      if (err) {
        console.log(err);
        res.status(400).end();
        return;
      }

      route.spots = [];
      req.body.spots.forEach(function(spot) {

        var routeSpot = {
          routeId: route.routeId,
          spotId: spot.spotId,
          userId: req.user.id,
          title: spot.title,
          desc: spot.desc,
          image: spot.image,
          geohash10: spot.geohash10
        };

        db.RouteSpot.create(routeSpot, function(err) {

          if (err) {
            console.log(err);
            res.status(400).end();
            return;
          }

          route.spots.push(spot);
          if (route.spots.length == req.body.spots.length) {

            res.json(route);

          }

        });
      });
    });
  });

router.route('/:routeId')
  .get(function(req, res, next) {

    db.Route
      .query(req.user.id)
      .where('routeId').equals(req.params.routeId)
      .exec(function(err, result) {

        if (err || !result.Items.length) {
          res.status(400).end();
          return;
        }

        var data = {
          routers: []
        };

        result.Items.forEach(function(item, index) {
          data.routers.push(item.attrs);
          data.routers[index].spots = [];

          db.RouteSpot
            .query(req.params.routeId)
            .attributes(['spotId', 'title', 'desc', 'image', 'geohash10'])
            .exec(function(err, result) {

              if (err || !result.Items.length) {
                console.log(err);
                res.status(400).end();
                return;
              }

              result.Items.forEach(function(spot) {
                data.routers[index].spots.push(spot.attrs);
              });
              res.json(data)

            });
        });
      });
  });

module.exports = router;