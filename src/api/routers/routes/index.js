'use strict';

var router = require('express').Router(),
    db = require('../../db');

router.route('/')
  .get(function(req, res, next) {

    var query;
    if (!req.query.geohash) {
      query = db.Route.scan()
    } else {
      query = db.Route
        .query(req.query.geohash.slice(0, 4))
        .usingIndex('RouteGeohash4GlobalIndex')
        .where('geohash10').beginsWith(req.query.geohash)
    }

    query.exec(function(err, result) {

      var data = {
        routes: []
      };

      result.Items.forEach(function(item) {
        data.routes.push(item.attrs);
      });
      res.json(data);

    });

  });


module.exports = router;
